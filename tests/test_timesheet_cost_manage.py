# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import unittest
import doctest
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.tests.test_tryton import suite as test_suite
from trytond.tests.test_tryton import doctest_teardown, doctest_checker
from trytond.pool import Pool
from trytond.exceptions import UserError
from trytond.modules.company.tests import create_company, set_company


class TimesheetCostManageTestCase(ModuleTestCase):
    """Test TimesheetCostManage module"""
    module = 'timesheet_cost_manage'

    @with_transaction()
    def test_cost_category_used(self):
        pool = Pool()
        Work = pool.get('timesheet.work')
        CostCategory = pool.get('cost.manage.category')
        ModelData = pool.get('ir.model.data')

        labor = CostCategory(ModelData.get_id('timesheet_cost_manage',
            'cost_category_labor'))
        cost_category = CostCategory(name='Cost Manage 1', parent=labor)
        cost_category.save()
        company = create_company()
        with set_company(company):
            work1 = Work(name='Work 1', cost_category=cost_category)
            work1.save()
        self.assertEqual(work1.cost_category_used, cost_category)

    @with_transaction()
    def test_cost_category_used_costs_category(self):
        pool = Pool()
        Work = pool.get('timesheet.work')
        Category = pool.get('timesheet.work.category')
        CostCategory = pool.get('cost.manage.category')
        ModelData = pool.get('ir.model.data')

        labor = CostCategory(ModelData.get_id('timesheet_cost_manage',
            'cost_category_labor'))
        cost_category = CostCategory(name='Cost 1', parent=labor)
        cost_category.save()
        category1, = Category.create([{
                    'name': 'Work Category 1',
                    'cost_category': cost_category,
                    'cost': True
                    }])
        company = create_company()
        with set_company(company):
            work1 = Work(name='Work 1', costs_category=True,
                cost_concept_category=category1)
            work1.save()
        self.assertEqual(work1.cost_category_used, cost_category)

    @with_transaction()
    def test_cost_category_used_error(self):
        Work = Pool().get('timesheet.work')

        company = create_company()
        with set_company(company):
            work1 = Work(name='Work 1', categories=())
            work1.save()
        with self.assertRaises(UserError):
            self.assert_(work1.cost_category_used)


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
        TimesheetCostManageTestCase))
    suite.addTests(doctest.DocFileSuite(
        'scenario_cost_manage.rst',
        tearDown=doctest_teardown, encoding='utf-8',
        checker=doctest_checker,
        optionflags=doctest.REPORT_ONLY_FIRST_FAILURE))
    suite.addTests(doctest.DocFileSuite(
        'scenario_teamless_works.rst',
        tearDown=doctest_teardown, encoding='utf-8',
        checker=doctest_checker,
        optionflags=doctest.REPORT_ONLY_FIRST_FAILURE))
    return suite
