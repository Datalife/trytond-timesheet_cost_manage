# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import fields
from trytond.pyson import Eval, Bool
from trytond.modules.cost_manage.cost_manage import CategorizedMixin


class Work(CategorizedMixin, metaclass=PoolMeta):
    __name__ = 'timesheet.work'

    own_cost_category = fields.Boolean('Own Cost Category',
        states={
            'invisible': Bool(Eval('costs_category'))
        })
    cost_concept_category = fields.Many2One('timesheet.work.category',
        'Cost Work Category', states={
            'required': Eval('costs_category', False)
        }, depends=['costs_category'])

    @classmethod
    def __setup__(cls):
        super(Work, cls).__setup__()
        cls.cost_category.states.update({
            'readonly': Bool(Eval('own_cost_category'))
            })
        cls.cost_category.depends.append('own_cost_category')

    @classmethod
    def create(cls, vlist):
        vlist = [x.copy() for x in vlist]
        for vals in vlist:
            if vals.get('own_cost_category'):
                work_name = vals.get('name')
                cost_category = cls.create_category(work_name)
                vals['cost_category'] = cost_category.id
        works = super(Work, cls).create(vlist)
        return works

    @classmethod
    def create_category(cls, name):
        pool = Pool()
        CostCategory = pool.get('cost.manage.category')
        ModelData = pool.get('ir.model.data')

        labor = CostCategory(ModelData.get_id('timesheet_cost_manage',
            'cost_category_labor'))
        cost_category = CostCategory(name=name, parent=labor)
        cost_category.save()
        return cost_category

    @classmethod
    def write(cls, *args):
        actions = iter(args)
        new_args = []
        for works, values in zip(actions, actions):
            if not values.get('own_cost_category', False):
                new_args.extend([works, values])
                continue
            for work in works:
                if work.own_cost_category or work.cost_category:
                    new_args.extend([[work], values])
                    continue
                name = values.get('name')
                if not name:
                    name = work.name
                cost_category = cls.create_category(name)
                new_values = values.copy()
                new_values['cost_category'] = cost_category.id
                new_args.extend([[work], new_values])
        super(Work, cls).write(*new_args)

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default['own_cost_category'] = None
        default['cost_category'] = None
        return super().copy(records, default=default)

    @fields.depends('costs_category')
    def on_change_costs_category(self):
        if self.costs_category:
            self.own_cost_category = False

    @classmethod
    def default_own_cost_category(cls):
        return True
