# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from trytond.pool import PoolMeta, Pool
from trytond.model import fields
from trytond.pyson import Eval
from trytond.modules.cost_manage.cost_manage import (CostSourceMixin,
    CostDocumentableMixin)
from itertools import groupby


class TeamTimesheetWork(CostSourceMixin, metaclass=PoolMeta):
    __name__ = 'timesheet.team.timesheet-timesheet.work'

    cost_amount = fields.Function(
        fields.Numeric('Cost amount',
            digits=(16, Eval('cost_amount_currency_digits', 2)),
            depends=['cost_amount_currency_digits']),
        'get_cost_amount')
    cost_amount_currency = fields.Function(
        fields.Many2One('currency.currency', 'Currency'),
        'get_cost_amount_currency_data')
    cost_amount_currency_digits = fields.Function(
        fields.Integer('Currency Digits'),
        'get_cost_amount_currency_data')
    cost_allocation_pattern = fields.Many2One(
        'cost.manage.cost.allocation.pattern', 'Cost allocation pattern',
        ondelete='RESTRICT', states={
            'readonly': (Eval('tts_state') != 'draft')
        },
        depends=['tts_state'])

    @classmethod
    def __setup__(cls):
        super(TeamTimesheetWork, cls).__setup__()
        cls.cost_override = fields.Function(cls.cost_override,
            'get_cost_override', setter='set_cost_override',
            searcher='search_cost_override')

    @classmethod
    def get_cost_amount_currency_data(cls, records, names):
        res = {}
        for r in records:
            for name in names:
                _name = name[21:]
                if not _name:
                    _name = 'id'
                res.setdefault(name, {})[r.id] = getattr(
                    r.team_timesheet.company.currency, _name)
        return res

    @classmethod
    def get_cost_amount(cls, records, name):
        pool = Pool()
        Line = pool.get('timesheet.line')

        res = dict.fromkeys(list(map(int, records)), None)
        lines = Line.search([
            ('tts_work', 'in', set(map(int, records)))],
            order=[('tts_work', 'ASC')])
        res.update({tts_work.id:
            tts_work.team_timesheet.company.currency.round(
                sum(tsl.cost_amount for tsl in work_lines))
            for tts_work, work_lines in groupby(lines,
                key=lambda x: x.tts_work)})
        return res

    def get_cost_category(self, name):
        return self.work.cost_category_used.id

    def get_cost_override(self, name=None):
        return self.team_timesheet.cost_override

    @classmethod
    def search_cost_override(cls, name, clause):
        return [('team_timesheet.%s' % name,) + tuple(clause[1:])]

    @classmethod
    def set_cost_override(cls, records, name, value):
        TeamTimesheet = Pool().get('timesheet.team.timesheet')
        team_timesheets = set(r.team_timesheet for r in records)
        TeamTimesheet.write(list(team_timesheets), {name: value})

    def _get_unique_key(self):
        key = super()._get_unique_key()
        if self.cost_allocation_pattern:
            key += (self.cost_allocation_pattern.id, )
        return key


class TeamTimesheetWork2(metaclass=PoolMeta):
    __name__ = 'timesheet.team.timesheet-timesheet.work'

    @classmethod
    def get_cost_amount(cls, records, name):

        res = {}
        with_timesheets = []
        for r in records:
            if r.team_timesheet.num_workers:
                cost_price = cost_price = r.work.compute_cost_price(
                    date=r.team_timesheet.date)
                res[r.id] = r.team_timesheet.company.currency.round(
                    Decimal(r.hours.total_seconds() / 3600)
                    * cost_price * r.team_timesheet.num_workers)
            else:
                with_timesheets.append(r)

        res.update(super().get_cost_amount(with_timesheets, name))
        return res


class TeamTimesheet(CostDocumentableMixin, metaclass=PoolMeta):
    __name__ = 'timesheet.team.timesheet'


class Timesheet(CostSourceMixin, metaclass=PoolMeta):
    __name__ = 'timesheet.line'

    cost_allocation_pattern = fields.Function(
        fields.Many2One('cost.manage.cost.allocation.pattern',
            'Cost allocation pattern'),
        'get_cost_allocation_pattern')

    def get_cost_allocation_pattern(self, name):
        if not self.team_timesheet:
            return None
        res = [x for x in self.team_timesheet.works if x == self.tts_work]
        if res:
            work, = res
            return (work.cost_allocation_pattern.id
                if work.cost_allocation_pattern else None)

    def get_cost_category(self, name):
        return self.work.cost_category_used.id
