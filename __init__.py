# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import team_timesheet
from . import work
from . import category


def register():
    Pool.register(
        team_timesheet.TeamTimesheetWork,
        team_timesheet.Timesheet,
        team_timesheet.TeamTimesheet,
        work.Work,
        category.Category,
        module='timesheet_cost_manage', type_='model')
    Pool.register(
        team_timesheet.TeamTimesheetWork2,
        module='timesheet_cost_manage', type_='model',
        depends=['timesheet_work_price'])
