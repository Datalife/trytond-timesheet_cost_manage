======================
Teamless Work Scenario
======================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from proteus import Model, Wizard
    >>> from trytond.modules.company.tests.tools import create_company, get_company
    >>> from trytond.modules.company_employee_team.tests.tools \
    ... import create_team, get_team
    >>> from decimal import Decimal
    >>> from datetime import timedelta
    >>> today = datetime.date.today()


Install timesheet_cost_manage::

    >>> config = activate_modules(['timesheet_cost_manage',
    ...     'timesheet_work_price'])


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create works::

    >>> Work = Model.get('timesheet.work')
    >>> work1 = Work(name='work1')
    >>> work1.save()
    >>> work2 = Work(name='work2')
    >>> cost_price = work2.cost_prices.new()
    >>> cost_price.work = work1
    >>> cost_price.cost_price = Decimal(10)
    >>> cost_price.date = today
    >>> work2.save()


Create team::

    >>> team = create_team()


Asign employee cost price::

    >>> for e in team.employees:
    ...     price = e.cost_prices.new()
    ...     price.date = today
    ...     price.cost_price = Decimal(8.50)
    ...     e.save()


Create a team timesheet::

    >>> TeamTimesheet = Model.get('timesheet.team.timesheet')
    >>> tts = TeamTimesheet(date=today, team=team)
    >>> tts_work = tts.works.new()
    >>> tts_work.work=work1
    >>> tts_work.time_type = 'employee'
    >>> tts_work.hours = timedelta(hours=8)
    >>> for _state in ('wait', 'confirm', 'do'):
    ...     tts.click(_state)
    >>> tts = TeamTimesheet(tts.id)
    >>> tts.works[0].cost_amount == 68.0
    True


Create a team timesheet without team::


    >>> tts = TeamTimesheet(date=today, num_workers=2)
    >>> tts_work = tts.works.new()
    >>> tts_work.work=work2
    >>> tts_work.hours = timedelta(hours=8)
    >>> for _state in ('wait', 'confirm', 'do'):
    ...     tts.click(_state)
    >>> tts = TeamTimesheet(tts.id)
    >>> tts.works[0].cost_amount == 160.0
    True
