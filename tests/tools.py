# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import datetime
from trytond.modules.team_timesheet.tests.tools import (
    create_team_timesheet as _create_team_timesheet)
from trytond.modules.cost_manage.tests.tools import (
    get_cost_allocation_pattern, create_cost_allocation_pattern)
from trytond.modules.company_employee_team.tests.tools import (
    create_team as _create_team,
    get_team)
from decimal import Decimal

__all__ = ['create_team_timesheet']


def create_team(
        name='A Team',
        parties_data=[{'name': 'Pepito Perez'}],
        price=Decimal('8.50'),
        date=None,
        config=None):
    team = _create_team(name=name, parties_data=parties_data, config=config)
    if not date:
        date = datetime.date.today()
    # Set cost to employees
    for e in team.employees:
        price_line = e.cost_prices.new()
        price_line.date = date
        price_line.cost_price = price
        e.save()

    return team


def create_team_timesheet(team=None, final_state='do', date=None, config=None):
    # Create team
    if not team:
        team = get_team()
    if not team:
        create_team(date=date)
        team = get_team()

    tts = _create_team_timesheet(team=team, final_state=final_state,
        date=date, config=config)

    # Set allocation pattern
    pattern = get_cost_allocation_pattern(config)
    if not pattern:
        create_cost_allocation_pattern(config)
        pattern = get_cost_allocation_pattern(config)
    tts.works[0].cost_allocation_pattern = pattern
    tts.save()

    return tts
