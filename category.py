# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.modules.cost_manage.cost_manage import CategorizedCategoryMixin


class Category(CategorizedCategoryMixin, metaclass=PoolMeta):
    __name__ = 'timesheet.work.category'
