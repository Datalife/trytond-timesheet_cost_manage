====================
Cost Manage Scenario
====================

Imports::

    >>> from trytond.tests.tools import activate_modules
    >>> from proteus import Model, Wizard
    >>> from trytond.modules.company.tests.tools import create_company, get_company
    >>> from trytond.modules.company_employee_team.tests.tools \
    ...	import create_team, get_team
    >>> from decimal import Decimal
    >>> from datetime import timedelta, datetime
    >>> today = datetime.today()


Install timesheet_cost_manage::

    >>> config = activate_modules('timesheet_cost_manage')


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Get today date::

    >>> import datetime
    >>> today = datetime.date.today()


Create works::

    >>> Work = Model.get('timesheet.work')
    >>> work1 = Work(name='work1')
    >>> work1.save()


Create team::

    >>> team = create_team()


Asign employee cost price::

	>>> for e in team.employees:
	...		price = e.cost_prices.new()
	...		price.date = today
	...		price.cost_price = Decimal(8.50)
	...		e.save()

Create cost allocation pattern::

    >>> CostAllocationPattern = Model.get('cost.manage.cost.allocation.pattern')

    >>> cost_allocation_pattern1 = CostAllocationPattern()
    >>> cost_allocation_pattern1.code = 'CAP 1'
    >>> cost_allocation_pattern1.name = 'CAP 1'
    >>> cost_allocation_pattern1.save()

    >>> cost_allocation_pattern2 = CostAllocationPattern()
    >>> cost_allocation_pattern2.code = 'CAP 2'
    >>> cost_allocation_pattern2.name = 'CAP 2'
    >>> cost_allocation_pattern2.save()

Create a team timesheet::

    >>> TeamTimesheet = Model.get('timesheet.team.timesheet')

    >>> tts = TeamTimesheet(date=today, team=team)
    >>> tts_work = tts.works.new()
    >>> tts_work.work=work1
    >>> tts_work.time_type = 'employee'
    >>> tts_work.hours = timedelta(hours=8)
    >>> for _state in ('wait', 'confirm', 'do'):
    ...		tts.click(_state)
    >>> tts = TeamTimesheet(tts.id)
 	>>> tts.works[0].cost_amount == 68.0
 	True

    >>> tts2 = TeamTimesheet(date=today, team=team)
    >>> tts_work = tts2.works.new()
    >>> tts_work.work=work1
    >>> tts_work.time_type = 'employee'
    >>> tts_work.hours = timedelta(hours=8)
    >>> tts_work.cost_allocation_pattern = cost_allocation_pattern1
    >>> tts_work = tts2.works.new()
    >>> tts_work.work=work1
    >>> tts_work.time_type = 'employee'
    >>> tts_work.hours = timedelta(hours=8)
    >>> tts_work.cost_allocation_pattern = cost_allocation_pattern1
    >>> tts2.save()
    Traceback (most recent call last):
     ...
    trytond.exceptions.UserError: "work1" work of teamtimesheet "2" must be unique. - 
    >>> tts_work = tts2.works[1]
    >>> tts_work.cost_allocation_pattern = cost_allocation_pattern2
    >>> tts2.save()
